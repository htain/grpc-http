package main

import (
	"context"
	"flag"
	"log"
	"test-grpc-http-same-port/pb"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	defaultName = "world"
)

var (
	addr = flag.String("addr", "localhost:8000", "the address to connect to")
	name = flag.String("name", defaultName, "name to greet")
)

func main() {
	flag.Parse()
	//set up a connection to the server
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGreeterClient(conn)

	//contact the server and print out its response
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.SayHello(ctx, &pb.HelloRequest{Name: *name})
	if err != nil {
		log.Fatal("Could not greet %v", err)
	}
	log.Printf("Greeting: %s", r.GetMessage())
}
