package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"test-grpc-http-same-port/pb"

	"github.com/gin-gonic/gin"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"google.golang.org/grpc"
)

type Handler struct {
	ginHandler     *gin.Engine
	grpcwebHandler *grpcweb.WrappedGrpcServer
}

type server struct {
	pb.UnimplementedGreeterServer
}

func main() {
	http.ListenAndServe(":8000", NewHandler())
}

func NewHandler() *Handler {
	router := gin.Default()
	router.UseH2C = true
	router.GET("/", gethelloworld)

	grpcServer := grpc.NewServer()
	pb.RegisterGreeterServer(grpcServer, &server{})
	wrappedServer := grpcweb.WrapServer(grpcServer)

	return &Handler{
		ginHandler:     router,
		grpcwebHandler: wrappedServer,
	}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// contentType := req.Header.Get("Content-Type")
	protoMajor := req.ProtoMajor
	if protoMajor == 2 {
		h.grpcwebHandler.ServeHTTP(w, req)
		return
	}
	h.ginHandler.ServeHTTP(w, req)
}

func gethelloworld(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "hello world"})
}

// implement sayhello
func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	fmt.Println("its here.")
	log.Printf("Received %v", in.GetName())
	return &pb.HelloReply{Message: "Hello " + in.GetName()}, nil
}
